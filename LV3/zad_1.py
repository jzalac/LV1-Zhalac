"""

############# Zhalac Josip ##############

"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('C:\Users\Nuxx\Desktop\LV\LV3\mtcars.csv')
df = pd.DataFrame(data=mtcars)

# Zadatak 1.1

resultMpg = df.sort(columns='mpg', ascending=False)
print resultMpg['mpg'].head(5)



#Zadatak 1.2

cyl_8 = mtcars[mtcars.cyl == 8]
resultCyl_8 = cyl_8.sort(columns='mpg', ascending=False).tail(3)
print resultCyl_8.tail(3)



#Zadatak 1.3

cyl_6 = mtcars[mtcars.cyl == 6]
resultCyl_6 = cyl_6.sort(columns='mpg', ascending=False)
print 'Srednja potro�nja automobila sa 6 cilindara: ',resultCyl_6['mpg'].mean()



#Zadatak 1.4

cyl_4_weight = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)]
print 'Srednja potro�nja automobila sa 4 cilindra i mase izmedu 2k i 2.2k: ', cyl_4_weight['mpg'].mean()



#Zadatak 1.5

automatic = mtcars[mtcars.am == 1]
manual = mtcars[mtcars.am == 0]

print 'Broj vozila sa automatskim mjenjacem:', len(automatic)
print 'Broj vozila sa rucnim mjenjacem:', len(manual)



#Zadatak 1.6

am_and_weight = mtcars[(mtcars.am == 1) & (mtcars.hp > 100)]
print 'Broj vozila sa automatskim mjenjacem i preko 100 hp-a: ', len(am_and_weight)



#Zadatak 1.7

mtcars.wt_new = mtcars.wt * 0.453592
print mtcars.wt_new