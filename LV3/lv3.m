clear all;
close all;
clc;

lambda2=4;
b1=0.000346073656051084;
b2=-0.000320147030454233;
a0=-0.997181263018302;
a1=2.99180943455715;
a2=-2.99462814442517;
p1=-1.9594;
p2=0.9602;
p3=-0.9602;
p4=-0.9220;

A=[0 1 0 0 0 b2;1 (a2-1) 0 0 b2 b1;(-1+a2) (a1-a2) 0 b2 b1 0; (a1-a2) (a0-a1) b2 b1 0 0; (a0-a1) -a0 b1 0 0 0; -a0 0 0 0 0 0];
P=[3*p4+p1+p3-a2+1; 3*p4^2+3*(p1+p3)*p4+p1*p3+p2-a1+a2; p4^3+3*p4^2*(p1+p3)+3*p4*(p1*p3+p2)+p2*p3-a0+a1; p4^3*(p1+p3)+3*p4^2*(p1*p3+p2)+3*p2*p3*p4+a0; p4^3*(p1*p3+p2)+3*p4^2*p2*p3; p4^3*p2*p3];
A1=inv(A);
Y=A1*P;

z=tf('z');
Rs=Y(1)+Y(2)*z+z^2;
Ss=Y(3)+Y(4)*z+Y(5)*z^2+Y(6)*z^3;
Am=(z^2*+p1*z+p2)*(z+p3);
C=z-1;
Am1=(1+p1+p2)*(1+p3);
Bminus1=b2+b1;
bm0=Am1/Bminus1;
P=Am*(z+p4)^3;

% proces
B1=[b2 b1 0];
A1=[1 a2 a1 a0];

% R,S,T
R=Am*C*Rs;
S=Am*Ss;
T=bm0*P;
[numr] = tfdata(R,'v');
[nums] = tfdata(S,'v');
[numt] = tfdata(T,'v');
Ts=0.01;