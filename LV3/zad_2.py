"""

############# Zhalac Josip ##############

"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('C:\Users\Nuxx\Desktop\LV\LV3\mtcars.csv')


# Zadatak 2.1

new_mtcars = mtcars.groupby('cyl')
new_mtcars.mean().mpg.plot(kind='bar', color='b')
plt.show()



# Zadatak 2.2

new_mtcars.mean().wt.plot(kind='box', color='r')
plt.show()



# Zadatak 2.3

new_mtcars_am = mtcars.groupby('am')
new_mtcars_am.mean().mpg.plot(kind='bar', color='g')



# Zadatak 2.4

new_mtcars_am = mtcars.groupby('am')
x = new_mtcars_am.mean()

fig = plt.figure()
ax = fig.add_subplot(111)
ax2 = ax.twinx()
width = 0.1

x.hp.plot(kind='bar', color='red', ax=ax, width=width, position=1)
x.qsec.plot(kind='bar', color='blue', ax=ax2, width=width, position=0)

plt.show()