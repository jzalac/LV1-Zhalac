# -*- coding: utf-8 -*-
"""

############# Zhalac Joszip ##############

"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from numpy.linalg import inv
from sklearn.metrics import r2_score

#Definiranje funkcija za mjerenu i stvarnu veličinu
def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
    
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

#Generiranje vektora od 100 uzoraka sa razmakom od 0.1
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

#Plotanje prvog grafa
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#Definiranje uzoraka x za treniranje te za testiranje
np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

#Plotanje drugog grafa
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

#Definiranje linearnog modela čiji su koeficijenti xtrain i ytrain
linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

#Definiranje pravca koji prolazi kroz skup
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)




#Zad 2.

xtrain_ones = np.ones(len(xtrain))
xtrain_ones = xtrain_ones[:, np.newaxis]
X=np.column_stack((xtrain_ones,xtrain)) #spajanje dva 1D vektora u jedan 2D (Jedinice i koef xtrain-a)

#Definiranje there po 4-10 formuli iz predloška
def machine_learning(X,ytrain):
    return np.dot(inv(np.dot(X.T,X)),(np.dot(X.T,ytrain)))
    
print machine_learning(X,ytrain)




#Zad 3.

theta = np.zeros(shape=(2, 1))
num_iter = 50
alpha = 0.01
J_history = np.zeros(shape=(num_iter, 1))  
m = len(X)

def compute_cost(X, xtrain, theta):

    predictions = X.dot(theta).flatten()
    sqErrors = (predictions - ytrain) ** 2

    J = (1.0 / (2 * m)) * sqErrors.sum()
    return J

def gradient_descent(X, ytrain, theta, alpha, num_iter):
    for i in range(0, num_iters):

        predictions = X.dot(theta).flatten()

        errors_x1 = (predictions - ytrain) * X[:, 0]
        errors_x2 = (predictions - ytrain) * X[:, 1]

        theta[0][0] = theta[0][0] - alpha * (1.0 / m) * errors_x1.sum()
        theta[1][0] = theta[1][0] - alpha * (1.0 / m) * errors_x2.sum()

        J_history[i, 0] = compute_cost(X, xtrain, theta)

    return J_history
    
gradient_descent(X, ytrain, theta, alpha, num_iter)
plt.figure()
plt.plot(range(0,num_iter),J_history)
plt.ylabel('J')
plt.xlabel('Broj iteracija')

print compute_cost(X, ytrain, theta)
print gradient_descent(X, ytrain, theta, alpha, num_iters)