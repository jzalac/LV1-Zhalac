#include <avr/io.h>

#define bit_set(p,m)((p)|=(m))
#define bit_flip(p,m)((p)^=(m))
#define BIT(x)(0x01<<(x))

unsigned long previousMillis=0;

const long interval=10;

void setup()
{

bit_set(DDRB,BIT(PB5));

}

void loop()
{

unsigned long currentMillis = millis();

if(currentMillis - previousMillis >= interval)
{
previousMillis = currentMillis;

bit_flip(PORTB,BIT(PB5));
}
}
