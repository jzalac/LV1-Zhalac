#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#define M 4

volatile unsigned int sinTimeCnt = 0;
int i;
volatile float ytrue = 0;

volatile float filter = 0;

volatile float y = 0;

volatile float ys = 0;

volatile float noise = 0;

const float Pi = 3.141593;

const float w = 10;

const float Ts = 0.01;

unsigned int noSamples = (2.0*Pi)/w/Ts;

volatile float uzorki[10]={0};

void setup()
{

Serial.begin(115200);
randomSeed(21);

cli();
TCCR1A=0;
TCCR1B=0;
OCR1A=20000;
TCCR1B |=(1<<WGM12);
TCCR1B |=(1<<CS11);
TIMSK1 |=(1<<OCIE1A);
sei();
}

void loop()
{
}

ISR(TIMER1_COMPA_vect)
{

ytrue=sin(10.0*sinTimeCnt*0.01)+2.0;
noise=(2.0*random(0,2)-1.0)*random(0,500)/1000.0;
filter=ytrue+noise;
for(i=0;i<M-1;i++)
  {
    filter+=uzorki[i];
  }
  filter/=M;

  for(i=M-2;i>=0;i--)
  {
    uzorki[i+1]=uzorki[i];
  }
  uzorki[0]=ytrue+noise;
  
Serial.print(ytrue,3);

Serial.write(" ");

Serial.println(ytrue+noise,3);

Serial.write(" ");

Serial.println(filter,3);

sinTimeCnt++;
if(sinTimeCnt>noSamples){
sinTimeCnt=0;
}
}
