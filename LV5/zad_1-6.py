# -*- coding: utf-8 -*-
"""

############# Zhalac Joszip ##############

"""
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as mat
from sklearn.preprocessing import PolynomialFeatures


def generate_data(n):
#prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
#druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
#permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data


#zad_1: Generiranje skupa za učenje i skupa za testiranje  
np.random.seed(242)
train_data=generate_data(200) 
np.random.seed(242)
test_data=generate_data(100)  



#zad_2: Prikaži generirane podatke za učenje u ravnini
plt.scatter(train_data[:,0],train_data[:,1], c=train_data[:,2], cmap='cool')



#zad_3: Model logisticke regresije
LogRegModel=lm.LogisticRegression()   
LogRegModel.fit(train_data[:,0:2],train_data[:,2]) 
theta0=LogRegModel.intercept_
theta1=LogRegModel.coef_

X2=(-theta0-theta1[:,0]*train_data[:,0])/theta1[:,1] 
X2=X2[:,np.newaxis]
plt.scatter(train_data[:,0],train_data[:,1], c=train_data[:,2], cmap='cool')
plt.plot(train_data[:,0],X2)  #prikazivanje granice odluke



#zad_4: Izlaz modela logisticke regresije - bojanje
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(train_data[:,0])-0.5:max(train_data[:,0])+0.5:.05,min(train_data[:,1])-0.5:max(train_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = LogRegModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="spring", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')



#zad_5: Klasifikacija testnog skupa podataka
ytest_predict=LogRegModel.predict(test_data[:,0:2]) 
ytest_predict=ytest_predict[:, np.newaxis]


xtest_predict=test_data[:,2] 
xtest_predict=xtest_predict[:, np.newaxis]
y=ytest_predict-xtest_predict



#zad_6: Matrica zabune
def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    
    width = len(c_matrix)
    height = len(c_matrix[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
            
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

c_mat=mat.confusion_matrix(xtest_predict,ytest_predict)
plot_confusion_matrix(c_mat)