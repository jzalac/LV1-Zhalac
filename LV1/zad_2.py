while True:
    try:
        value = float(raw_input("Unesite broj u intervalu od 0.0 do 1.0: "))
    except ValueError:
        print("Broj nije unesen!")
        continue

    if value < 0:
        print("Vaš broj nije u zadanom intervalu!")
        continue
    elif value > 1:
        print("Vaš broj nije u zadanom intervalu!")
        continue
    else:
        #Izlaz iz petlje.
        break
    
if value >= 0.9:
    print 'Broj pripada kategoriji: A'

elif value >= 0.8:
    print 'Broj pripada kategoriji: C'

elif value >= 0.7:
    print 'Broj pripada kategoriji: C'

elif value >= 0.6:
    print 'Broj pripada kategoriji: D'

elif value < 0.6:
    print 'Broj pripada kategoriji: F'
