total = 0.0
inputValue = None
minValue = None
maxValue = None
midValue = None

while True:
    inputValue = raw_input('Unesite broj: ')
    if inputValue == 'Done':     # Ako je uneseno 'Done', zaustavi petlju.
        break
    try:
        num = float(inputValue)
        total += num              #total = total + num
        midValue = total / 2
        
    except ValueError:
        print 'Krivi unos broja, pokušajte ponovno!'
        continue
    
    if minValue is None or maxValue is None:
        minValue = num
        maxValue = num
    elif num < minValue:
        minValue = num
    elif num > maxValue:
        maxValue = num
        
print "Maksimalni iznos brojeva: ", total
print "Maksimalna vrijednost: ", maxValue
print "Minimalna vrijednost", minValue
print "Srednja vrijednost: ", midValue
