# -*- coding: utf-8 -*-
"""

############# Zhalac Joszip ##############


"""


import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as mat
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import learning_curve
from sklearn.preprocessing import scale
import sklearn.linear_model as lm
from sklearn.preprocessing import PolynomialFeatures

def generate_data(n):


     #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
     #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
     #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    
     #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data
    
def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest') 
 
    width = len(c_matrix)
    height = len(c_matrix[0]) 
 
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
            
def krivulja_ucenja(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training podatci")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc=4)
    return plt

def krivulja_odluke(estimator, naslov,x,y, polyFeaturesRequired=False):
    h=.02
    # create a mesh to plot in
    x_min, x_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    y_min, y_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    if polyFeaturesRequired==True:
        coeff=poly.fit_transform(np.c_[xx.ravel(),yy.ravel()])
    else:
        coeff=np.c_[xx.ravel(), yy.ravel()]
    # testiranje svakog dijela kreiranog mesha
    fig, ax = plt.subplots(figsize=(8, 6))
    Z = estimator.predict(coeff)
    
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    ax.contourf(xx, yy, Z, cmap=plt.cm.Paired)
    ax.axis('off')
    
    # Plot also the training points
    ax.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.Paired)
    
    ax.set_title(naslov)

np.random.seed(12)
data=generate_data(1000)
dataTrain=data[0:int(np.floor(len(data)*0.7))]
x=data[int(np.floor(len(data)*0.7)):]
TrainS=scale(dataTrain[:,0:2])
TestS=scale(x[:,0:2])
ANN=MLPClassifier(hidden_layer_sizes=(3,10), max_iter=700)
ANN.fit(TrainS, dataTrain[:,2])
y=ANN.predict(TestS)


matrix=confusion_matrix(x[:,2],y)

plot_confusion_matrix(matrix)

conf_mat=matrix
accuracy=mat.accuracy_score(x[:,2],y)
missclass=1-accuracy
prec=mat.precision_score(x[:,2],y)
recall=mat.recall_score(x[:,2],y)


print "MultiLayer:"
print "Tocnost: ", accuracy
print "Ucestalost pogresne klasifikacije: " , missclass
print "Preciznost: ",  prec
print "Odziv: ",  recall

krivulja_odluke(ANN, 'MultiLayer',TestS[:,0:2],y)

logRegress=lm.LogisticRegression(max_iter=700)
poly=PolynomialFeatures(degree=2,include_bias=False)
polyTrain=poly.fit_transform(dataTrain[:,0:2])
polyTest=poly.fit_transform(x[:,0:2])
logRegress.fit(polyTrain,dataTrain[:,2])
yRegress=logRegress.predict(polyTest)
krivulja_odluke(logRegress, 'Logisticka regresija',polyTest,yRegress,True)
matrix=confusion_matrix(x[:,2],yRegress)
plot_confusion_matrix(matrix)

conf_mat=matrix
accuracy=mat.accuracy_score(x[:,2],yRegress)
missclass=1-accuracy
prec=mat.precision_score(x[:,2],yRegress)
recall=mat.recall_score(x[:,2],yRegress)

print "Linear:"
print "Tocnost: ", accuracy
print "Ucestalost pogresne klasifikacije: " , missclass
print "Preciznost: ",  prec
print "Odziv: ",  recall