clear all;
close all;
clc;

Ts=0.02;
zeta=0.7;
omega=20;
omegaest=40;
j=sqrt(-1);

%zrk
p1=exp((-zeta*omega+j*omega*sqrt(1-zeta^2))*Ts);
p2=exp((-zeta*omega-j*omega*sqrt(1-zeta^2))*Ts);
p3=exp(-2*omega*zeta*Ts);
p4=exp(-2.1*zeta*omega*Ts);

%estimator
pes1=exp((-zeta*omegaest-j*omegaest*sqrt(1-zeta^2))*Ts);
pes2=exp((-zeta*omegaest+j*omegaest*sqrt(1-zeta^2))*Ts);
pes3=exp(-2*omegaest*zeta*Ts);
pes4=exp(-2.1*zeta*omegaest*Ts);
pes5=exp(-2.2*omegaest*zeta*Ts);

A=[0 1 0 0; -1000 -11 1000 6;0 0 0 1; 1000 6 -1000 -11];
B=[0 2 0 0]';
C=[0 0 1 0];
D=[0]';
Ts=0.02;

sysd=ss(A,B,C,D,Ts);
[fi,gama,c,d]=c2dm(A,B,C,D,Ts,'zoh');


L=place(fi,gama,[p1 p2 p3 p4]);


K=place(fi',c',[pes1 pes2 pes3 pes4])';

[brojnik,nazivnik]=ss2tf(fi-gama*L,gama,c,d);
pf=(sum(brojnik))/(sum(nazivnik));


x0=[0.05 0 0.05 0];

AA = [sysd.A sysd.B; 0 0 0 0 1];
BB = [sysd.B;0];
CC = [sysd.C 0];
DD = sysd.D;
sysd2 = ss(AA,BB,CC,DD,Ts);

Aw = 1
Axw = sysd.B
 
L = L
Lw = 1
 
Ka = place(sysd2.A', sysd2.C', [pes1 pes2 pes3 pes4 pes5])';
Kw = Ka(5,1)
Ka = [K(1,1);K(2,1);K(3,1);K(4,1);]