import numpy as np
import matplotlib.pyplot as plt

mpg=np.genfromtxt("C:\Users\Gauss user\Desktop\LV\LV2\mtcars.csv",delimiter=',',usecols=[1],skip_header=1)
hp=np.genfromtxt("C:\Users\Gauss user\Desktop\LV\LV2\mtcars.csv",delimiter=',',usecols=[4],skip_header=1)       
weight=np.genfromtxt("C:\Users\Gauss user\Desktop\LV\LV2\mtcars.csv",delimiter=',',usecols=[6],skip_header=1)      

plotFig = plt.figure(1)                                           
ax = plt.subplot(111)                                 
plt.plot(mpg,hp,'b .')                             
plt.xlabel('Miles per gallon')
plt.ylabel('Horse power') 